package com.example.shoppingbroadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class ProductReceiver() : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if(intent.action == context.getString(R.string.action_product_added)) {
            val notificationId = intent.getIntExtra("notification_id", -1)
            if (notificationId == -1) return
            val serviceIntent = Intent(context, ProductAddService::class.java)
            serviceIntent.putExtra("channel_id", intent.getStringExtra("channel_id"))
            serviceIntent.putExtra("channel_name", intent.getStringExtra("channel_name"))
            serviceIntent.putExtra("notification_id", intent.getIntExtra("notification_id", -1))
            serviceIntent.putExtra("id", intent.getIntExtra("id", -1))
            serviceIntent.putExtra("name", intent.getStringExtra("name"))
            serviceIntent.putExtra("price", intent.getDoubleExtra("price", -1.0))
            serviceIntent.putExtra("amount", intent.getIntExtra("amount", -1))
            serviceIntent.putExtra("isBought", intent.getBooleanExtra("isBought", false))
            context.startForegroundService(serviceIntent)
        }
    }
}