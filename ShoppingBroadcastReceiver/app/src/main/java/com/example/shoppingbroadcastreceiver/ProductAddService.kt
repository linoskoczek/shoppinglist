package com.example.shoppingbroadcastreceiver

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat


class ProductAddService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        var chanId = intent!!.getStringExtra("channel_id")
        val chanName = intent.getStringExtra("channel_name")
        if(chanId != null && chanName != null) {
            createChannel(chanId, chanName)
        } else {
            chanId = createChannel("channel_default", "Default channel")
        }

        val notificationId = intent.getIntExtra("notification_id", -1)
        val toProductAddActivityIntent = Intent()
        toProductAddActivityIntent.component =
            ComponentName("com.example.shoppinglist", "com.example.shoppinglist.activities.ProductAddActivity")
        toProductAddActivityIntent.putExtra("channel_id", intent.getStringExtra("channel_id"))
        toProductAddActivityIntent.putExtra("channel_name", intent.getStringExtra("channel_name"))
        toProductAddActivityIntent.putExtra("notification_id", notificationId)
        toProductAddActivityIntent.putExtra("id", intent.getIntExtra("id", -1))
        toProductAddActivityIntent.putExtra("name", intent.getStringExtra("name"))
        toProductAddActivityIntent.putExtra("price", intent.getDoubleExtra("price", -1.0))
        toProductAddActivityIntent.putExtra("amount", intent.getIntExtra("amount", -1))
        toProductAddActivityIntent.putExtra("isBought", intent.getBooleanExtra("isBought",  false))
        toProductAddActivityIntent.action = notificationId.toString()
        toProductAddActivityIntent.setPackage("com.example.shoppingbroadcastreceiver")
        toProductAddActivityIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        val pendingIntent = PendingIntent.getActivity(
            applicationContext, 0, toProductAddActivityIntent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val notification = NotificationCompat.Builder(this, chanId)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle("New product added (" + intent.getIntExtra("id", -1).toString() + ")")
            .setContentText(intent.getStringExtra("name"))
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)

        NotificationManagerCompat
            .from(this)
            .notify(notificationId, notification.build())

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createChannel(channel_id: String, channel_name: String) : String {
        val notificationChannel = NotificationChannel(
            channel_id,
            channel_name,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        NotificationManagerCompat.from(this).createNotificationChannel(notificationChannel)
        return channel_id
    }
}