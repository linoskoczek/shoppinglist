package com.example.shoppinglist.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.shoppinglist.R
import com.example.shoppinglist.models.FavouriteShop
import com.example.shoppinglist.repositories.FavouriteShopRepository
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.fragment_add_favourite_shop.*

class FavouriteShopsActivity : AppCompatActivity() {
    private lateinit var favouriteShopsRepository : FavouriteShopRepository
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var latitude : Double = -1.0
    private var longitude : Double = -1.0


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_shops)
        setSupportActionBar(findViewById(R.id.toolbar))
        favouriteShopsRepository = FavouriteShopRepository.instance
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getLocationUpdates()

        btnAddNewFavouriteShop.setOnClickListener {
            if (this.latitude != -1.0 && this.longitude != -1.0) {
                val newFavouriteShop = FavouriteShop(
                    name = editTextNewFavouriteShopName.text.toString(),
                    description = editTextNewFavouriteShopDescription.text.toString(),
                    coordinateX = this.latitude,
                    coordinateY = this.longitude,
                    radius = seekBarRadius.progress
                )
                favouriteShopsRepository.insert(newFavouriteShop)

                editTextNewFavouriteShopName.setText("")
                editTextNewFavouriteShopDescription.setText("")
                seekBarRadius.progress = 5
            } else {
                Toast.makeText(this, "Cannot retrieve current location!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun getLocationUpdates()
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest()
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 1000
        locationRequest.smallestDisplacement = 50f
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY //set according to your app function
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                if (locationResult.locations.isNotEmpty()) {
                    this@FavouriteShopsActivity.latitude = locationResult.lastLocation.latitude
                    this@FavouriteShopsActivity.longitude = locationResult.lastLocation.longitude
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }
}