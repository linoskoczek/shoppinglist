package com.example.shoppinglist.models

import com.google.firebase.database.IgnoreExtraProperties
import java.util.*

@IgnoreExtraProperties
data class Product (
    var id: String = UUID.randomUUID().toString(),
    var name: String = "",
    var price: Double = 0.0,
    var amount: Int = 0,
    var isBought : Boolean = false
)