package com.example.shoppinglist.activities

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.example.shoppinglist.R
import com.example.shoppinglist.models.Product
import com.example.shoppinglist.repositories.ProductRepository
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_product_add.*
import kotlin.random.Random

class ProductAddActivity : AppCompatActivity() {
    private val productRepository = ProductRepository.instance

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_add)

        val price = intent.getDoubleExtra("price", -1.0)
        val amount = intent.getIntExtra("amount", -1)
        edit_new_product_name.setText(intent.getStringExtra("name"))
        edit_new_product_price.setText(if (price == -1.0) "" else price.toString())
        edit_new_product_amount.setText(if (amount == -1) "" else amount.toString())
        edit_new_product_is_bought.isChecked = intent.getBooleanExtra("isBought", false)

        if(price != -1.0) {
            btn_add_product.text = getString(R.string.update)
        }
        btn_add_product.setOnClickListener {
            val newProduct = Product(
                name = edit_new_product_name.text.toString(),
                price = edit_new_product_price.text.toString().toDouble(),
                amount = edit_new_product_amount.text.toString().toInt(),
                isBought = edit_new_product_is_bought.isChecked
            )
            val oldId = intent.getStringExtra("id")
            if(oldId != null) {
                newProduct.id = oldId
            }
            if(validateProduct(newProduct) && validateInputs()) {
                if(oldId == null) {
                    productRepository.insert(newProduct)
                } else {
                    productRepository.update(newProduct)
                }
                finish()
            } else {
                Snackbar.make(it, "Invalid data provided", Snackbar.LENGTH_LONG).show()
                val inputMethodManager: InputMethodManager =
                    getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(it.applicationWindowToken, 0)
            }
        }
    }

    private fun validateProduct(product: Product): Boolean {
        return product.amount > 0 &&
                product.price >= 0.0 &&
                product.name.isNotEmpty()
    }

    private fun validateInputs(): Boolean {
        return edit_new_product_name.text.toString().isNotEmpty() &&
                edit_new_product_amount.text.toString().toInt() > 0 &&
                edit_new_product_price.text.toString().toDouble() >= 0.0
    }

    private fun createBroadcastOnProductAdded(product: Product) {
        val broadcast = Intent()
        broadcast.action = getString(R.string.action_product_added)
        broadcast.component =
            ComponentName(
                "com.example.shoppingbroadcastreceiver",
                "com.example.shoppingbroadcastreceiver.ProductReceiver"
            )
        broadcast.setPackage("com.example.shoppingbroadcastreceiver")
        broadcast.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        broadcast.putExtra("channel_id", "product_added_channel")
        broadcast.putExtra("channel_name", "Product added channel")
        broadcast.putExtra("notification_id", Random.nextInt(0, 999999999))
        broadcast.putExtra("name", product.name)
        broadcast.putExtra("price", product.price)
        broadcast.putExtra("amount", product.amount)
        broadcast.putExtra("isBought", product.isBought)
        sendBroadcast(broadcast, "com.example.shoppinglist.ON_PRODUCT_ADDED_PERMISSION")
    }
}