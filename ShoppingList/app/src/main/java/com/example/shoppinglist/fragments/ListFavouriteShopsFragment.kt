package com.example.shoppinglist.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppinglist.FavouriteShopsListAdapterFirebase
import com.example.shoppinglist.R
import com.example.shoppinglist.interfaces.ISession
import com.example.shoppinglist.models.FavouriteShop
import com.example.shoppinglist.repositories.FavouriteShopRepository
import com.example.shoppinglist.utilities.SessionManager
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.fragment_list_favourite_shops.*

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ListFavouriteShopsFragment : Fragment() {
    private lateinit var adapter : FavouriteShopsListAdapterFirebase
    private lateinit var favouriteShopsRepository : FavouriteShopRepository
    private lateinit var session : ISession

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_favourite_shops, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        session = SessionManager.instance

        val recyclerView = favourite_shops_recyclerview
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        favouriteShopsRepository = FavouriteShopRepository.instance

        val options = FirebaseRecyclerOptions.Builder<FavouriteShop>()
            .setQuery(session.getFirebaseDatabaseRef().child("favouriteshops"), FavouriteShop::class.java)
            .build()

        adapter = FavouriteShopsListAdapterFirebase(options)
        recyclerView.adapter = adapter
    }


    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }
}