package com.example.shoppinglist.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shoppinglist.*
import com.example.shoppinglist.interfaces.ISession
import com.example.shoppinglist.models.Product
import com.example.shoppinglist.utilities.SessionManager
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.activity_product_list.*
import kotlinx.android.synthetic.main.content_product_list.*


class ProductListActivity : AppCompatActivity() {
    private lateinit var session : ISession
    private lateinit var adapter : ProductListAdapterFirebase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        setSupportActionBar(findViewById(R.id.toolbar))
        session = SessionManager.instance
        val recyclerView = product_recycler_view
        recyclerView.layoutManager = LinearLayoutManager(this)

        val options = FirebaseRecyclerOptions.Builder<Product>()
            .setQuery(session.getFirebaseDatabaseRef().child("products"), Product::class.java)
            .build()

        adapter = ProductListAdapterFirebase(options)
        recyclerView.adapter = adapter

        btnAddProduct.setOnClickListener {
            val intent = Intent(this@ProductListActivity, ProductAddActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        adapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        adapter.stopListening()
    }
}