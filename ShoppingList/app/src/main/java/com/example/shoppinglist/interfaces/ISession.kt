package com.example.shoppinglist.interfaces

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference

interface ISession {
    fun getFirebaseAuth() : FirebaseAuth
    fun getFirebaseDatabaseRef() : DatabaseReference
    fun signOut()
}