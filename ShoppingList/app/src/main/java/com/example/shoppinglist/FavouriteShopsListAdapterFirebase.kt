package com.example.shoppinglist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.models.FavouriteShop
import com.example.shoppinglist.repositories.FavouriteShopRepository
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions

class FavouriteShopsListAdapterFirebase(options: FirebaseRecyclerOptions<FavouriteShop>) :
    FirebaseRecyclerAdapter<FavouriteShop, FavouriteShopsListAdapterFirebase.ShopViewHolder>(options) {
        private val shopRepository = FavouriteShopRepository.instance

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
            return ShopViewHolder.create(parent)
        }

        class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            private val shopId: TextView = itemView.findViewById(R.id.shop_id)
            private val shopName: TextView = itemView.findViewById(R.id.shop_name)
            private val shopDescription: TextView = itemView.findViewById(R.id.shop_description)
            private val shopCoordinateX: TextView = itemView.findViewById(R.id.shop_coordinatex)
            private val shopCoordinateY: TextView = itemView.findViewById(R.id.shop_coordinatey)
            private val shopRadius: TextView = itemView.findViewById(R.id.shop_radius)

            fun bind(shop : FavouriteShop) {
                shopId.text = shop.id
                shopName.text = shop.name
                shopDescription.text = shop.description
                shopCoordinateX.text = "Coordinate X: " + shop.coordinateX.toString()
                shopCoordinateY.text = "Coordinate Y: " + shop.coordinateY.toString()
                shopRadius.text = "Radius: " + shop.radius.toString()
            }

            companion object {
                fun create(parent: ViewGroup): ShopViewHolder {
                    val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.favourite_shop_layout, parent, false)
                    return ShopViewHolder(view)
                }
            }
        }

        override fun onBindViewHolder(holder: ShopViewHolder, position: Int, shop: FavouriteShop) {
            val current = getItem(position)
            holder.bind(current)

            holder.itemView.setOnClickListener {
                shopRepository.delete(current)
            }
        }
    }