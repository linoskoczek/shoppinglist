package com.example.shoppinglist.utilities

import com.example.shoppinglist.interfaces.ISession
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class SessionManager private constructor(): ISession {
    private lateinit var auth : FirebaseAuth
    private var database : DatabaseReference? = null

    companion object {
        val superInstance = SessionManager()
        val instance : ISession  = superInstance as ISession
    }

    override fun getFirebaseAuth(): FirebaseAuth = auth

    override fun getFirebaseDatabaseRef(): DatabaseReference {
        if(database == null) {
            throw Exception("Not logged in, so no database reference was created!")
        } else {
            return database as DatabaseReference
        }
    }

    override fun signOut() {
        auth.signOut()
        database = null
    }

    fun setAuth(auth: FirebaseAuth) {
        this.auth = auth
        database = FirebaseDatabase.getInstance().getReference(auth.currentUser!!.uid)
    }
}