package com.example.shoppinglist.repositories

import androidx.lifecycle.ViewModel
import com.example.shoppinglist.interfaces.ISession
import com.example.shoppinglist.models.FavouriteShop
import com.example.shoppinglist.utilities.SessionManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class FavouriteShopRepository private constructor() : ViewModel() {
    private var session : ISession = SessionManager.instance
    private val childName = "favouriteshops"

    companion object {
        val instance = FavouriteShopRepository()
    }

    fun getAll(callback: (list: List<FavouriteShop>) -> Unit) : List<FavouriteShop> {
        val list : MutableList<FavouriteShop> = mutableListOf()

        session.getFirebaseDatabaseRef().child(childName)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(snapshotError: DatabaseError) {
                    TODO("not implemented")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val children = snapshot.children
                    children.forEach {
                        val value = it.getValue(FavouriteShop::class.java)
                        if (value != null) {
                            list.add(value)
                        }
                    }
                    callback(list)
                }
            })
        return list
    }

    fun insert(shop: FavouriteShop) {
        session.getFirebaseDatabaseRef().child(childName).push().setValue(shop)
    }

    fun update(shop: FavouriteShop) {
        session.getFirebaseDatabaseRef().child(childName).orderByChild("id")
            .equalTo(shop.id).addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach { it.ref.setValue(shop) }
                }
                override fun onCancelled(error: DatabaseError) = Unit
            })
    }

    fun delete(shop: FavouriteShop) {
        session.getFirebaseDatabaseRef().child(childName).orderByChild("id")
            .equalTo(shop.id).addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach { it.ref.removeValue() }
                }
                override fun onCancelled(error: DatabaseError) = Unit
            })
    }
}
