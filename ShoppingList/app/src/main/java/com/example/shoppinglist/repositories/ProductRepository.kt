package com.example.shoppinglist.repositories

import androidx.lifecycle.ViewModel
import com.example.shoppinglist.interfaces.ISession
import com.example.shoppinglist.models.Product
import com.example.shoppinglist.utilities.SessionManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class ProductRepository private constructor() : ViewModel() {
    private var session : ISession = SessionManager.instance
    private val childName = "products"

    companion object {
        val instance = ProductRepository()
    }

    fun insert(product: Product) {
        session.getFirebaseDatabaseRef().child(childName).push().setValue(product)
    }

    fun update(product: Product) {
        session.getFirebaseDatabaseRef().child(childName).orderByChild("id")
            .equalTo(product.id).addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach { it.ref.setValue(product) }
                }
                override fun onCancelled(error: DatabaseError) = Unit
            })
    }

    fun delete(product: Product) {
        session.getFirebaseDatabaseRef().child(childName).orderByChild("id")
            .equalTo(product.id).addListenerForSingleValueEvent(object: ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    snapshot.children.forEach { it.ref.removeValue() }
                }
                override fun onCancelled(error: DatabaseError) = Unit
            })
    }
}
