package com.example.shoppinglist.models

import java.util.*

data class FavouriteShop (
    var id: String = UUID.randomUUID().toString(),
    var name : String = "",
    var description : String = "",
    var coordinateX : Double = -1.0,
    var coordinateY : Double = -1.0,
    var radius : Int = -1,
)