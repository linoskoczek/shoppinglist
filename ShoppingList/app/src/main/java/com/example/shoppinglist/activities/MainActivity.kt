package com.example.shoppinglist.activities

import android.Manifest
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.shoppinglist.R
import com.example.shoppinglist.interfaces.ISession
import com.example.shoppinglist.utilities.Preferences
import com.example.shoppinglist.utilities.SessionManager
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val preferences = Preferences(this)
    private lateinit var session: ISession
    private var geofenceList : ArrayList<Geofence> = ArrayList()
    private var geofencePendingIntent: PendingIntent? = null
    private lateinit var geofencingClient : GeofencingClient


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = SessionManager.instance
        setContentView(R.layout.activity_main)
        addOnClickListeners()
    }


    private fun addOnClickListeners() {
        btnOpenSettings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        btnOpenShoppingList.setOnClickListener {
            startActivity(Intent(this, ProductListActivity::class.java))
        }

        btnOpenFavouriteShopsList.setOnClickListener {
            startActivity(Intent(this, FavouriteShopsActivity::class.java))
        }

        btnOpenFavouriteShopsMap.setOnClickListener {
            startActivity(Intent(this, FavouriteShopsMapActivity::class.java))
        }

        btnLogOut.setOnClickListener {
            session.signOut();
            startActivity(Intent(this, LoginActivity::class.java))
        }

        requestLocationPermissions()
    }

    private fun requestLocationPermissions() {
        val perms = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            arrayOf(
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        } else {
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        }

        var requestCode = 0
        while (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            requestCode += 1
            requestPermissions(perms, requestCode)
        }
    }

    override fun onStart() {
        super.onStart()
        txtHomeTitle.text = preferences.welcomeText
        if(!preferences.szopState) {
            imageSzop.visibility = View.INVISIBLE;
        } else {
            imageSzop.visibility = View.VISIBLE;
        }
    }
}
