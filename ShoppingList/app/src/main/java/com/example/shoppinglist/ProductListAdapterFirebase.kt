package com.example.shoppinglist

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.shoppinglist.activities.ProductAddActivity
import com.example.shoppinglist.models.Product
import com.example.shoppinglist.repositories.ProductRepository
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.product_layout.view.*

class ProductListAdapterFirebase(options: FirebaseRecyclerOptions<Product>) :
    FirebaseRecyclerAdapter<Product, ProductListAdapterFirebase.ProductViewHolder>(options) {
    private val productRepository = ProductRepository.instance

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder.create(parent)
    }

    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val productName: TextView = itemView.findViewById(R.id.product_name)
        private val productAmount: TextView = itemView.findViewById(R.id.product_amount)
        private val productPrice: TextView = itemView.findViewById(R.id.product_price)
        private val productIsBought: CheckBox = itemView.findViewById(R.id.product_is_bought)

        fun bind(product : Product) {
            productName.text = product.name
            productAmount.text = product.amount.toString()
            productPrice.text = product.price.toString() + " PLN"
            productIsBought.isChecked = product.isBought
        }

        companion object {
            fun create(parent: ViewGroup): ProductViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.product_layout, parent, false)
                return ProductViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int, product: Product) {
        val current = getItem(position)
        holder.bind(current)

        holder.itemView.setOnClickListener {
            productRepository.delete(current)
        }

        holder.itemView.setOnLongClickListener {
            val intent = Intent(holder.itemView.context, ProductAddActivity::class.java)
            intent.putExtra("id", current.id)
            intent.putExtra("name", current.name)
            intent.putExtra("amount", current.amount)
            intent.putExtra("price", current.price)
            intent.putExtra("isBought", current.isBought)
            startActivity(holder.itemView.context, intent, null)
            true
        }

        holder.itemView.product_is_bought.setOnClickListener {
            product.isBought = !product.isBought
            productRepository.update(product)
        }
    }
}
