package com.example.shoppinglist.utilities

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor


class Preferences(private val context: Context) {
    private fun open(): SharedPreferences {
        return context.getSharedPreferences("settings", Context.MODE_PRIVATE)
    }

    private fun edit(): Editor {
        return open().edit()
    }

    var welcomeText: String
    get() = open().getString("welcomeText", "Szoping List!") as String
    set(welcomeText){
        val edit = edit()
        edit.putString("welcomeText", welcomeText)
        edit.apply()
    }

    var szopState: Boolean
        get() = open().getBoolean("isSzopTurnedOn", true)
        set(isSzopTurnedOn) {
            val edit = edit()
            edit.putBoolean("isSzopTurnedOn", isSzopTurnedOn);
            edit.apply()
        }
    

}