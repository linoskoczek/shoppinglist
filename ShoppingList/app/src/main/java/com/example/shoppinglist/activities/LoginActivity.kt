package com.example.shoppinglist.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.shoppinglist.R
import com.example.shoppinglist.utilities.SessionManager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        addLoginRegisterListeners()
    }

    private fun addLoginRegisterListeners() {
        val auth = FirebaseAuth.getInstance()
        btnLogin.setOnClickListener {
            auth.signInWithEmailAndPassword(
                editTextEmailAddress.text.toString(),
                editTextPassword.text.toString()
            ).addOnCompleteListener {
                if (it.isSuccessful) {
                    Snackbar.make(
                        findViewById(android.R.id.content),
                        "Logged in successfully!",
                        Snackbar.LENGTH_LONG
                    ).show()
                    SessionManager.superInstance.setAuth(auth)
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                } else {
                    Toast.makeText(
                        findViewById(android.R.id.content),
                        "Incorrect login or password!",
                        Toast.LENGTH_LONG
                    ).show()
                    Log.e("Login error", it.exception?.message!!)
                    Log.e("Login error", it.exception?.stackTrace.toString())
                }
            }
        }

        btnRegister.setOnClickListener {
            auth.createUserWithEmailAndPassword(
                editTextEmailAddress.text.toString(),
                editTextPassword.text.toString()
            ).addOnCompleteListener {
                if (it.isSuccessful) {
                    Snackbar.make(
                        findViewById(android.R.id.content),
                        "Registered successfully!",
                        Snackbar.LENGTH_LONG
                    ).show()
                } else {
                    Snackbar.make(
                        findViewById(android.R.id.content),
                        "Failed to register!",
                        Snackbar.LENGTH_LONG
                    ).show()
                    Log.e("Login error", it.exception?.message!!)
                    Log.e("Login error", it.exception?.stackTrace.toString())
                }
            }
        }
    }
}