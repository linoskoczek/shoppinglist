package com.example.androidwidgetexample

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.SoundPool
import android.net.Uri
import android.widget.RemoteViews
import java.util.*

class NewAppWidget : AppWidgetProvider() {
    companion object {
        private var sounds: SoundPool? = null
        private var isLoaded = false
        private var previousSoundId = 0
        private var currentSoundId = 0
        private var currentImageId = 0
        private val soundsList: MutableList<Int> = ArrayList()
        private val imageIdList: List<Int> = listOf(R.drawable.pjwstk, R.drawable.alpaca)
    }
    private lateinit var widgetManager : AppWidgetManager

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        widgetManager = appWidgetManager
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {}

    override fun onDisabled(context: Context) {}

    override fun onReceive(context: Context?, intent: Intent) {
        super.onReceive(context, intent)
        when (intent.action) {
            "playOnClick" -> {
                createSoundPool(context!!)
                playSomeSound(sounds)
                println(currentSoundId)
            }
            "nextOnClick" -> {
                createSoundPool(context!!)
                currentSoundId++
                if (currentSoundId >= soundsList.size) currentSoundId = 0
                updateSoundId(context)
            }
            "previousOnClick" -> {
                createSoundPool(context!!)
                currentSoundId--
                if (currentSoundId < 0) currentSoundId = soundsList.size - 1
                updateSoundId(context)
            }
            "changeImage" -> {
                currentImageId++
                if (currentImageId >= imageIdList.size) currentImageId = 0
                updateImage(context!!)
            }
        }
    }

    private fun updateSoundId(context : Context) {
        val views = RemoteViews(context.packageName, R.layout.new_app_widget)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisWidget = ComponentName(context, NewAppWidget::class.java)
        views.setTextViewText(R.id.textSoundId, "Sound ID: $currentSoundId")
        appWidgetManager.updateAppWidget(thisWidget, views)
    }

    private fun updateImage(context : Context) {
        val views = RemoteViews(context.packageName, R.layout.new_app_widget)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisWidget = ComponentName(context, NewAppWidget::class.java)
        views.setImageViewResource(R.id.imageView, imageIdList[currentImageId])
        appWidgetManager.updateAppWidget(thisWidget, views)
    }
    private fun playSomeSound(sounds: SoundPool?): Unit {
        if(!isLoaded) {
            sounds!!.setOnLoadCompleteListener { s, id, status ->
                isLoaded = true
                previousSoundId = s.play(soundsList[currentSoundId], 1F, 1F, 1, 0, 1f);
            }
        } else {
            previousSoundId = sounds!!.play(soundsList[currentSoundId], 1F, 1F, 1, 0, 1f);
        }
    }

    private fun createSoundPool(context: Context) {
        if (sounds == null) {
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()

            sounds = SoundPool.Builder()
                .setAudioAttributes(attributes)
                .setMaxStreams(1)
                .build()

            soundsList.add(sounds!!.load(context, R.raw.challoct, 1))
            soundsList.add(sounds!!.load(context, R.raw.example, 1))
        }
    }
}

internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int,
) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.data = Uri.parse("https://pja.edu.pl")
    val openBrowserIntent =
        PendingIntent.getActivity(context, appWidgetId, intent, PendingIntent.FLAG_CANCEL_CURRENT)

    val playIntent = createPendingIntent(context, "playOnClick")
    val nextIntent = createPendingIntent(context, "nextOnClick")
    val previousIntent = createPendingIntent(context, "previousOnClick")
    val changeImage = createPendingIntent(context, "changeImage")

    val views = RemoteViews(context.packageName, R.layout.new_app_widget)
    views.setOnClickPendingIntent(R.id.btnOpenWebsite, openBrowserIntent)
    views.setOnClickPendingIntent(R.id.btnPlay, playIntent)
    views.setOnClickPendingIntent(R.id.btnNext, nextIntent)
    views.setOnClickPendingIntent(R.id.btnPrevious, previousIntent)
    views.setOnClickPendingIntent(R.id.imageView, changeImage)

    val intentUpdate = Intent(context, NewAppWidget::class.java)
    intentUpdate.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE

    appWidgetManager.updateAppWidget(appWidgetId, views)
}

internal fun createPendingIntent(context: Context, actionName: String): PendingIntent? {
    val intent = Intent(actionName)
    intent.component = ComponentName(context, NewAppWidget::class.java)
    return PendingIntent.getBroadcast(context, 0, intent, 0)
}